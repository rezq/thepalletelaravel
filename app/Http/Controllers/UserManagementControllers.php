<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserManagementControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::orderBy('name', 'asc')->get();

        return view('layouts.backend.users.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'form add user';
        $roles = Role::all();


        return view('layouts.backend.users.add' , compact('title','roles'));
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'role_id'=> 'required',
            'address' => 'required',
            'phone'=> 'required'
        ]);
        
         User::create([
            'name' => $request->nama,
            'email'=> $request->email,
            'password'=> bcrypt('password'),
            'role_id'=> $request-> role_id,
            'address'=> $request->address,
            'phone'=> $request->phone
            
        ]);


        return redirect('/users')->with('success', 'data berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id', $id)->first();
        $roles = Role::all();

        
        return view('layouts.backend.users.edit', compact('data','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'name' => 'required',
            'email' => 'required',
            'role_id'=> 'required',
            'address' => 'required',
            'phone'=> 'required'
        ]);

        $data = User::where('id', $id)->first();
        $data->update([
            'name' => $request->name,
            'email' => $request->email,
            'role_id'=>  $request->role_id,
            'address'=> $request->address,
            'phone' => $request->phone

        ]);
        

       return redirect('/users')->with('success', 'data berhasil di update');
    }

    public function profile(){

        $userProfile = User::where('id', Auth()->user()->id)->first();


        return view('layouts.backend.users.profile', compact('userProfile'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $data = User::where('id', $id)->delete();
        return redirect()->back()->with('success', 'data berhasil di hapus..!');
       
    }


    public function resetUser(Request $request, $id){
       $data = User::where('id', $id)->first();
        $newPass = $data->email;
       $data->update([
           'password' => Hash::make($newPass)
       ]);

       return redirect()->back()->with('success', 'password berhasil di reset ke- '. $newPass);
    }
}
