<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Http\Request;

class OrderControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $key = $request->search;
        if ($request->has('search')) {
         
           $data = Transaction::whereHas('user', function($q) use ($key){
                   $q->where('name','like', '%' . $key.'%');
               })->orWhere('status_order', 'like', '%' . $key . '%')
                ->orWhere('invoice', 'like', '%' . $key . '%')
                ->get();

                 return view('layouts.backend.Orders' , compact('data'));
        }
  
        $data = Transaction::orderBy('created_at','asc')->get();
        return view('layouts.backend.Orders' , compact('data'));
    }

    public function search($key){
           
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmPay($id){
        $data = Transaction::where('id', $id)->first();
        $data->update([
            'status_order'=> 'PAID'
        ]);

        return redirect()->back()->with('success' , 'Orderan Berhasil di Konfirmasi');
    }

    public function sendOrder($id){
        $data = Transaction::where('id', $id)->first();
        $data->update([
            'status_order' => 'SEND'
        ]);

        return redirect()->back()->with('success' , 'Orderan Sedang di Kirim ke alamat Customer');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $check = Transaction::where('id', $id)->first();

        return view('layouts.backend.Orders' , compact('check'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Transaction::where('id', $id)->first();

        return view('layouts.backend.edit_order', [
            'data'=> $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Transaction::where('id', $id)->first();

        $data->update([
            'status_order' => $request->status_order
        ]);

  

        return redirect('/orders')->with('success', 'data berhasil di update..!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Transaction::where('id', $id)->delete();

        return redirect()->back()->with('success', 'data berhasil di hapus..!');

    }
}
