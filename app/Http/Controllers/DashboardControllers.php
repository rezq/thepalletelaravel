<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardControllers extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $title = 'Dashboard';
        $user = User::where('role_id', 3)->get();
        $product = Product::all();
        $orderp = Transaction::where('status_order', 'UNPAID')->get();
        $orderSend = Transaction::where('status_order', 'PAID')->orderBy('created_at', 'asc')->get();
        $ordersuccess = Transaction::where('status_order', 'SUCCESS')->get();

        return view('layouts.backend.dashboard', compact('title', 
        'user', 
        'product',
        'orderp',
        'ordersuccess',
        'orderSend'));
    }
}
