<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Product Page';
        $key = $request->search;
        if ($request->has('search')) {
            $data = Product::whereHas('category', function($q) use ($key){
                   $q->where('nama','like', '%' . $key.'%');
               })->orWhereHas('status', function($q) use ($key){
                   $q->where('nama', 'like', '%'. $key . '%');
               })
               ->orWhere('nama', 'like', '%' . $key . '%')
                ->orWhere('price', 'like', '%' . $key . '%')
                ->get();

             return view('layouts.backend.products', [
                        'title' => $title,
                        'data' => $data,
                        ]);
        }
        $data = Product::with(['category', 'status'])->get();

        return view('layouts.backend.products', [
            'title' => $title,
            'data' => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dtcategory = Category::all();
        $dtStatus = Status::all();
        return view('layouts.backend.add_product', [
            'data' => $dtcategory,
            'status' => $dtStatus,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgThumb = $request->file('imgthumb');
        $fileImg1 = $request->file('img1');
        $fileImg2 = $request->file('img2');

        if ($imgThumb) {
            $filName = $request->nama . ' ' . $imgThumb->getClientOriginalName();
            $path = $imgThumb->storeAs('images/', $filName);
        } else {
            $path = null;
        }

        if ($fileImg1) {
            $fileName1 = $request->nama . ' ' . $fileImg1->getClientOriginalName();

            $path1 = $fileImg1->storeAs('images/', $fileName1);
        } else {
            $path1 = null;
        }
        if ($fileImg2) {
            $fileName2 = $request->nama . ' ' . $fileImg2->getClientOriginalName();

            $path2 = $fileImg2->storeAs('images/', $fileName2);
        } else {
            $path2 = null;
        }

        $data = Product::create([
            'nama' => $request->nama,
            'price' => $request->price,
            'category_id' => $request->category_id,
            'desc' => $request->desc,
            'img_thumb' => $path,
            'img1' => $path1,
            'img2' => $path2,
            'status_id' => $request->status_id,
        ]);

        return redirect('/products')->with('success','data berhasil di tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::find($id);

        return view('layouts.backend.detail_product', [
            'data' => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Product::find($id);
        $dtcategory = Category::all();
        $dtStatus = Status::all();

        return view('layouts.backend.edit_product', [
            'dtcategory' => $dtcategory,
            'dtStatus' => $dtStatus,
            'data' => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Product::find($id);

        $imgThumb = $request->file('imgthumb');
        $fileImg1 = $request->file('img1');
        $fileImg2 = $request->file('img2');

        if ($imgThumb) {
            \Storage::delete($data->img_thumb);
            $filName = $request->nama . ' ' . $imgThumb->getClientOriginalName();
            $path = $imgThumb->storeAs('images', $filName);
        } else {
            $path = $data->img_thumb;
        }

        if ($fileImg1) {
            \Storage::delete($data->img1);
            $fileName1 = $request->nama . ' ' . $fileImg1->getClientOriginalName();

            $path1 = $fileImg1->storeAs('images', $fileName1);
        } else {
            $path1 = $data->img1;
        }
        if ($fileImg2) {
            \Storage::delete($data->img2);
            $fileName2 = $request->nama . ' ' . $fileImg2->getClientOriginalName();

            $path2 = $fileImg2->storeAs('images', $fileName2);
        } else {
            $path2 = $data->img2;
        }

        $data->update([
            'nama' => $request->nama,
            'price' => $request->price,
            'category_id' => $request->category_id,
            'desc' => $request->status_id,
            'img_thumb' => $path,
            'img1' => $path1,
            'img2' => $path2,
        ]);

        return redirect()
            ->back()
            ->with('success', 'data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Product::find($id);
        $data->delete();
        \Storage::delete([$data->img_thumb, $data->img1, $data->img2]);

        return redirect()
            ->back()
            ->with('success', 'data berhasil di delete');
    }

    /**
     * Display a listing of the resource API.
     *
     * @return \Illuminate\Http\Response
     */

  
}
