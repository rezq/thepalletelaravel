<?php

namespace App\Http\Controllers\API;

use App\Models\Cart;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TransactionController extends Controller
{
      public function indexApi()
    {
        try {   
        $data = Product::all()->random(6);

        return response(
            [
                'message' => 'Success',
                'data' => $data,
            ], 200);
   
          
        } catch (\Exception $e) {
            return response(
                [
                    'message' => 'Something went wrong!',
                    'error' => $e,
                ],
                500,
            );
        }
    }
     public function listALl()
    {
        try {   
        $data = Product::all();

        return response(
            [
                'message' => 'Success',
                'data' => $data,
            ], 200);
   
          
        } catch (\Exception $e) {
            return response(
                [
                    'message' => 'Something went wrong!',
                    'error' => $e,
                ],
                500,
            );
        }
    }

    public function filterKrawangan(){
        try {
            $data = Product::with('category')->where('category_id', '1')->get();

            return response()->json([
                'message' => 'success',
                'data' => $data
            ],200);
        } catch (\Throwable $th) {
           return response()->json([
               'message' => 'something went wrong..!'
           ],500);
        }
    }
    
    public function filterLampu(){
        try {
            $data = Product::with('category')->where('category_id', '2')->get();

            return response()->json([
                'message' => 'success',
                'data' => $data
            ],200);
        } catch (\Throwable $th) {
           return response()->json([
               'message' => 'something went wrong..!'
           ],500);
        }
    }
     public function filterKantor(){
        try {
            $data = Product::with('category')->where('category_id', '3')->get();

            return response()->json([
                'message' => 'success',
                'data' => $data
            ],200);
        } catch (\Throwable $th) {
           return response()->json([
               'message' => 'something went wrong..!'
           ],500);
        }
    }

     public function filterSculpture(){
        try {
            $data = Product::with('category')->where('category_id', '4')->get();

            return response()->json([
                'message' => 'success',
                'data' => $data
            ],200);
        } catch (\Throwable $th) {
           return response()->json([
               'message' => 'something went wrong..!'
           ],500);
        }
    }

    public function searchApi($key){
        try {
            if ($key != null) {
                 $data = Product::whereHas('category', function($q) use ($key){
                   $q->where('nama','like', '%' . $key.'%');
               })->orWhere('nama', 'like', '%' . $key . '%')
                ->orWhere('price', 'like', '%' . $key . '%')
                ->get();
            
                return response()->json(
                [
                        'message' => 'Success',
                        'data' => $data,
                    ],200);
            }
            $data = Product::with('category')->all();
               return response()->json(
                [
                        'message' => 'Success',
                        'data' => $data,
                    ],200);
            
        
        } catch (\Exception $e) {
            
            return response()->json([
                'message' => 'something went wrong!',
                'error' => $e
            ],500);
        }
    }

    public function showApi($id)
    {
        try {
            $data = Product::where('id', $id)->first();
            $stats = $data->status->nama;
            $cat = $data->category->nama;
            return response()->json(
                [
                    'message' => 'Success',
                    'data' => $data,
                    $stats,
                    $cat,
                ],
                200,
            );
        } catch (\Exception $e) {
            return response(
                [
                    'message' => 'Data not Found!',
                    'error' => $e,
                ],
                404,
            );
            
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $data = User::where('email', $request->email)->first();

        if (!$data || !Hash::check($request->password, $data->password)) {
            return response()->json(
                [
                    'message' => 'Unauthorize login',
                    'status' => 'Gagal Login',
                ],
                401,
            );
        }

        $cart = User::with('transactions')
            ->where('id', $data->id)
            ->first();

        $token = $data->createToken('authToken')->plainTextToken;

        return response()->json(
            [
                'message' => 'Success',
                'data' => $cart,
                'token' => $token,
            ],
            200,
        );
    }
     /**
     * Display the specified resource.
     *
     * @param  int  $id form user where status_order Unpaid
     * @return \Illuminate\Http\Response
     */
    public function getProfile($id){
       try {
            $data = User::where('id', $id)->first();

             return response()->json([
                 'message' => 'success',
                 'data' => $data
             ],200);
       } catch (\Exception $e) {
           return response()->json([
               'message' => 'data not found..!'
           ],404);
       }
    }
    public function userProfile(Request $request, $id){
      try {
            $data = User::where('id', $id)->first();
            $data->email  = $request->email ? $request->email : $data->email;
            $data->name = $request->name ? $request->name : $data->name;
            $data->address= $request->address ? $request->address : $data->address;
            $data->phone = $request->phone ? $request->phone : $data->phone;
            $data->update();
     

            return response()->json([
                'message' => 'success',
                'data' => $data
            ],200);
      } catch (\Exception $e) {
          return response()->json([
                'message' => 'something went wrong!',
                'data' => $e
            ],500);
      }
    }

    public function changePass(Request $request, $id){
        try {
            $data = User::where('id', $id)->first();
            $data->password = $request->password;
            $data->save();

            return response()->json([
                'message'=> 'success'
            ],200);
        } catch (\Exception $e) {
           return response()->json([
               'message' => 'something went wrong'
           ],400);
        }
    }
    public function newCustomer(Request $request){

        try {
          

        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role_id'=> 3,
            'address' => $request->address,
            'phone' => $request->phone,
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'message' => 'success',
            'data' => $data
        ], 200);


        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Something went wrong!',
                'error' => $e
            ], 500);
        }


    }

    public function orderUnpaid($id){
        
      try {
        
        $data = Transaction::with(['product'])->where('user_id', $id)
                ->where('status_order', 'UNPAID')
                ->get();

            return response()->json([
                'message' => 'success',
                'data' => $data
            ], 200);
      } catch (\Throwable $e) {
          
          return response()->json([
              'message' => 'Something went wrong!',
              'error' => $e,
          ], 404);
      }

    }

 /**
     * Display the specified resource.
     *
     * @param  int  $id from user where status_order Paid
     * @return \Illuminate\Http\Response
     */
      public function orderPaid($id){
     
        try {
           $data = Transaction::with(['product'])->where('user_id', $id)
                ->where('status_order','!=' ,'UNPAID')->where('status_order', '!=','FINISH')
                ->get();
           
           return response()->json([
               "message" => "success",
               "data" => $data,
           ], 200);
        } catch (\Exception $e) {
          
          return response()->jswon([
              "message" => "something went wrong!",
              "error" =>  $e
          ], 400);
        }

    }

    public function orderSend($id){
    try {
           $data = Transaction::with(['product'])->where('user_id', $id)
                ->where('status_order', 'SEND')
                ->get();
           
           return response()->json([
               "message" => "success",
               "data" => $data,
           ], 200);
        } catch (\Exception $e) {
          
          return response()->jswon([
              "message" => "something went wrong!",
              "error" =>  $e
          ], 400);
        }

    }

    public function get($id){
        $data = Transaction::where('id', $id)->first();
        $data->status_order = 'FINISH';
        $data->save();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id from user where status_order Paid
     * @return \Illuminate\Http\Response
     */
      public function orderFinish($id){
     
        try {
           $data = Transaction::with(['product'])->where('user_id', $id)
                ->where('status_order', 'FINISH')
                ->get();

          
            
                return response()->json([
                    "message" => "success",
                    "data" => $data,
                ], 200);
        
           
        
        } catch (\Exception $e) {
          
          return response()->jswon([
              "message" => "something went wrong!",
              "error" =>  $e
          ], 400);
        }

    }

    public function randProduct()
    {
        try {
            $data = Product::all()->random(6);

            return response()->json(
                [
                    'message' => 'success',
                    'data' => $data,
                ],
                200,
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => 'Something went wrong!',
                    'error' => $e,
                ],
                500,
            );
        }
    }

    public function buyItem(Request $request)
    {
        try {
            $buy = new Transaction();
            $buy->user_id = $request->user_id;
            $buy->product_id = $request->product_id;
            $buy->qty = $request->qty;
            $buy->price = $request->price;
            $buy->total = $request->price * $request->qty;
            $buy->note = $request->note;
            $buy->tanggal = Carbon::now()->format('D-m-Y');
            $buy->status_order = 'UNPAID';
            $buy->invoice = $buy->invoice();
            $buy->save();

            return response()->json(
                [
                    'message' => 'success',
                ],
                200,
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => 'something went wrong',
                    'error' => $e,
                ],
                500,
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //Todo get userID from request
            $orderUser = Order::where('user_id', Auth::user()->id)->first();

            //?? Check Validasi data Exist or Not
            //** If data store doesn't exist then store data to database*/

            $order = new Order();
            if (empty($orderUser)) {
                $order->user_id = Auth::user()->id;
                $order->grand_total = 0;
                $order->save();
            }
            //Todo add to Cart
            $datacart = Cart::where('checkout_id', $order->id)
                ->where('product_id', $request->product_id)
                ->first();

            $cart = new Cart();

            $cart->product_id = $request->product_id;
            $cart->user_id = Auth::user()->id;
            $cart->checkout_id = $request->checkout_id;
            $cart->price = $request->price;
            $cart->qty = $request->qty;
            $cart->total = $cart->price * $request->qty;
            $cart->save();

            //Todo grandtotal
            $grandTotal = Order::where('id', $request->checkout_id)->first();
            $grandTotal->grand_total = $cart->sumTotal();
            $grandTotal->update();

            return response()->json(
                [
                    'message' => 'success',
                ],
                200,
            );
        } catch (\Exception $e) {
            //Todo catch Error & send Message
            return response()->json(
                [
                    'message' => 'Something went wrong!',
                    'error' => $e,
                ],
                400,
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
