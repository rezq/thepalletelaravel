<?php

namespace App\Models;

use App\Models\Transaction;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'phone',
        'role_id'
    ];

    public function avatar($size = 100){
        $default ="wavatar";

        // return "https://avatar.oxro.io/avatar.svg?name=".$this->email."&background=ff6b6b&caps=3&bold=true";
      return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $this->email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

    }

    public function transactions(){

        return $this->hasMany(Transaction::class, 'user_id', 'id');
    }

    public function cart(){

        return $this->hasMany(Cart::class, 'user_id', 'id');
    }

    public function checkout(){

        return $this->hasMany(Order::class,'user_id','id');
    }

    public function role(){

        return $this->hasOne(Role::class, 'id','role_id')->withDefault();
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
