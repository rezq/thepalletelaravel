<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table= 'checkout';

    protected $hidden = ['created_at','updated_at'];

    public function cart(){

        return $this->hasMany(Cart::class ,'checkout_id');
    }

    public function invoice(){
        $date = date('dMy');
        $code  = 'INV/';
        $uniqeId = $this->user_id;
        $char = "abcdefghijklmnopqrstuvwxyz";
        $charsplit = str_split($char);
        $resultChar = "";
        for ($i=0; $i <3 ; $i++) { 
            $randItem = array_rand($charsplit);
            $resultChar .= $charsplit[$randItem];
        }
        
        $result = $code . $date.'/'.$resultChar.'/'.$uniqeId.'PL';

        return $result;
    }

    
}
