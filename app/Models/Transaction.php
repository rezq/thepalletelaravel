<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transaction';

    protected $fillable= [
        'user_id',
        'product_id',
        'qty',
        'price',
        'total',
        'tanggal',
        'note',
        'status_order',
        'invoice'

    ];

    public function user(){
      return $this->belongsTo(User::class, 'user_id','id');
    }

      public function product(){

        return $this->belongsTo(Product::class, 'product_id')->withDefault();
    }

      // public function products(){
      //   return $this->hasMany(Product::class, 'product_id');
      // }

      public function invoice(){
        $date = date('dMy');
        $code  = 'INV/';
        $uniqeId = $this->user_id;
        $char = "abcdefghijklmnopqrstuvwxyz";
        $charsplit = str_split($char);
        $resultChar = "";
        for ($i=0; $i <3 ; $i++) { 
            $randItem = array_rand($charsplit);
            $resultChar .= $charsplit[$randItem];
        }
        
        $result = $code . $date.'/'.$resultChar.'/'.$uniqeId.'PL';

        return $result;
    }

}
