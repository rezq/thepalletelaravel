<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $fillable = ['nama', 'price', 'category_id', 'desc', 'img_thumb', 'img1', 'img2', 'status_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function transactions(){
        return $this->hasMany(Transaction::class, 'product_id',);
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function getThumbnail()
    {
        return '/images/' . $this->img_thumb;
    }

    public function getImage1()
    {
        return '/images/' . $this->img1;
    }
    public function getImage2()
    {
        return '/images/' . $this->img2;
    }
}
