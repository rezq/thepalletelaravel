<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    use HasFactory;


    protected $table = 'ulasan';

    //TODO allow data input

    protected $fillable = [
        'user_id',
        'desc'
    ];


    public function transaction(){

        return $this->belongsTo(Transaction::class , 'ulasan_id');
    }
}
