<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $table = 'cart';

    protected $hidden =['created_at','updated_at'];

    public function product(){

        return $this->belongsTo(Product::class, 'product_id');
    }

    public function order(){

        return $this->belongsTo(Order::class,'checkout_id', 'id');
    }

    public function sumTotal(){
        $pesanan = $this->checkout_id;
        $total = Cart::where('checkout_id', $pesanan)->sum('total');

        return $total;
    }
}
