<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('nama', 255);
            $table->integer('price');
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->text('desc')->nullable();
            $table->string('img_thumb')->nullable();
            $table->string('img1')->nullable();
            $table->string('img2')->nullable();
            $table->bigInteger('status_id')->nullable()->unsigned();

            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
