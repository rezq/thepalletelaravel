<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Product::class;
    public function definition()
    {
        return [
            'nama' => $this->faker->word(3),
            'price' => $this->faker->price(),
            'category_id' => 1,
            'desc'=> $this->faker->sentence(),
            'status_id'=> 2,
            
        ];
    }
}
