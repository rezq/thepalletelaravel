<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Product;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

  
    public function run()
    {
          $faker = Faker::create('id_ID');

         DB::table('products')->insert([
             [  
             'nama' => 'Krawangan Kaca',
             'price' => '40000',
             'category_id'=> 1,
             'desc' => $faker->sentence(5),
             'status_id'=> 2,
             'created_at'=> Carbon::now(),
             'updated_at'=>Carbon::now()
             ],
            [  
             'nama' => 'Krawangan Kayu',
             'price' => '20000',
             'category_id'=> 1,
             'desc' => $faker->sentence(5),
             'status_id'=> 2,
             'created_at'=> Carbon::now(),
             'updated_at'=> Carbon::now()
             ],
             [  
             'nama' => 'Krawangan batu',
             'price' => '30000',
             'category_id'=> 1,
             'desc' => $faker->sentence(5),
             'status_id'=> 2,
             'created_at'=> Carbon::now(),
             'updated_at'=> Carbon::now()
             ]

         ]);
        //
    }
}
