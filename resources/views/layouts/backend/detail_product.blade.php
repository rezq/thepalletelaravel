@extends('layouts.backend.app')
@section('content')



    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">

                    <div class="card-header p-0 mx-5 mt-5 position-relative z-index-1">
                        <div class="row mb-4">
                            <div class="col-md-3">
                                <a href="{{ url('/products') }}" class="btn btn-default text-md"><i
                                        class="fas fa-long-arrow-alt-left"></i></a>
                            </div>


                        </div>
                        <div class="row d-flex  ">
                            <div class="col-md">
                                <a href="javascript:;" class="d-block">
                                    @if (empty($data->img_thumb))

                                        <img src="https://placeholder.pics/svg/600x500/DEDEDE/555555/Image%20Belum%20Ada"
                                            class="img-fluid border-radius-lg">
                                    @else

                                        <img src="{{ $data->getThumbnail() }}" class="img-fluid border-radius-lg">
                                    @endif
                                </a>

                            </div>
                            <div class="col-md">
                                <span
                                    class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">{{ $data->category->nama }}</span>
                                <a href="javascript:;" class="card-title h5 d-block text-darker">
                                    <h3>{{ $data->nama }}</h3>
                                </a>
                                <h5 class="card-description mb-4">
                                    Harga : Rp. {{ number_format($data->price, 0) }}
                                </h5>
                                @if ($data->status_id == 1)
                                    <span class="badge badge-sm bg-gradient-danger">{{ $data->status->nama }}</span>
                                @elseif($data->status_id == 2)
                                    <span class="badge badge-sm bg-gradient-success">{{ $data->status->nama }}</span>
                                @else
                                    <span class="badge badge-sm bg-gradient-warning">{{ $data->status->nama }}</span>
                                @endif
                                <a href="javascript:;" class="card-title h5 d-block text-darker mt-4">
                                    Description
                                </a>
                                <p class="card-description mt-4">
                                    Use border utilities to quickly style the border and border-radius of an element. Great
                                    for
                                    images, buttons.
                                </p>
                                <div class="row d-flex justify-content-end">
                                    <div class="col-md-6">

                                        <a href="{{ url('/delete/' . $data->id . '/product') }}"
                                            class="btn btn-danger btn-sm ">Delete</a>
                                        <a href="{{ url('/edit/' . $data->id . '/product') }}"
                                            class="btn btn-dark btn-sm ">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-body mx-4 mt-3 pt-2">

                        <div class="author align-items-center">
                            <div class="col-md-3">
                                @if (empty($data->img1))
                                    <img src="https://placeholder.pics/svg/60x60/DEDEDE/555555/Image%20Belum%20Ada"
                                        class="img-fluid border-radius-lg">
                                @else
                                    <img src="{{ $data->getImage1() }}" alt="..." class="avatar shadow mx-1"
                                        style="width: 60px; height:60px;">
                                @endif
                                @if (empty($data->img2))
                                    <img src="https://placeholder.pics/svg/60x60/DEDEDE/555555/Image%20Belum%20Ada"
                                        class="img-fluid border-radius-lg">
                                @else
                                    <img src="{{ $data->getImage2() }}" alt="..." class="avatar shadow mx-1"
                                        style="width: 60px; height:60px;">
                                @endif



                            </div>
                            <div class="col-md-3">

                            </div>
                            {{-- <div class="name ps-3">
                                <span>Mathew Glock</span>
                                <div class="stats">
                                    <small>Posted on 28 February</small>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
