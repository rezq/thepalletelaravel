@extends('layouts.backend.app')
@section('content')



    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        @if (session('success'))
                            <div class="alert alert-success text-white" role="alert">
                                <strong>Success!</strong> {{ session('success') }}
                            </div>
                        @endif
                        <div class="col-md-4">

                        </div>
                    </div>
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-6 ms-2">
                            <a href="{{ url('/add') }}" class="btn btn-outline-dark btn-sm ">tambah</a>
                        </div>
                        <div class="col-md-4 me-3">
                            <form action="{{ url('/orders') }}" method="GET">
                                @csrf
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Search.."
                                        aria-label="Recipient's username" aria-describedby="button-addon2" name="search">
                                    <button class="btn btn-outline-primary mb-0" type="submit" id="button-addon2"><i
                                            class="fas fa-search" aria-hidden="true"></i></span></button>
                                </div>
                            </form>


                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Customer Name</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Total Belanja</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Product Name</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Status Payment</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Action </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $dt)
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="{{ $dt->user->avatar() }}" class="avatar avatar-sm me-3"
                                                            alt="user1">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $dt->user->name }}</h6>

                                                        <p class="text-xs text-secondary mb-0">
                                                            {{ $dt->invoice }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0">
                                                    Rp.{{ number_format($dt->total, 0) }}</p>
                                                {{-- {{-- <p class="text-xs text-secondary mb-0">Organization</p> --}}
                                            </td>

                                            <td class="align-middle text-center">
                                                <img src="{{ $dt->product->getThumbnail() }}"
                                                    class="avatar avatar-sm me-3" alt="user1">
                                                <a href="{{ url('show/' . $dt->id . '/product') }}">
                                                    <p class="mb-0 text-sm">
                                                        <span class="text-secondary text-xs font-weight-bold">
                                                            {{ $dt->product->nama }}
                                                        </span>
                                                    </p>
                                                </a>

                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                @if ($dt->status_order == 'UNPAID')
                                                    <span
                                                        class="badge badge-sm bg-gradient-danger">{{ $dt->status_order }}</span>
                                                @elseif($dt->status_order == 'PAID')
                                                    <span class="badge badge-sm"
                                                        style="background: rgb(78, 78, 243)">{{ $dt->status_order }}</span>
                                                @elseif($dt->status_order == 'SEND')
                                                    <span
                                                        class="badge badge-sm bg-gradient-success ">{{ $dt->status_order }}</span>
                                                @else
                                                    <span
                                                        class="badge badge-sm bg-gradient-info">{{ $dt->status_order }}</span>

                                                @endif
                                            </td>
                                            <td class="align-middle text-center">
                                                <a href="{{ url('/update-order/' . $dt->id) }}"
                                                    class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                                                    data-original-title="Edit user">
                                                    Update
                                                </a>
                                                |<a href="{{ url('/delete-order/' . $dt->id) }}"
                                                    class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                                                    data-original-title="Edit user"
                                                    onclick="if (! confirm('Yakin ingin menghapus data ini?')) { return false; }">
                                                    Delete
                                                </a>

                                            </td>

                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Detail Order
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            ...
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
