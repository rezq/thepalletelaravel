@extends('layouts.backend.app')
@section('content')



    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        @if (session('success'))
                            <div class="alert alert-success text-white" role="alert">
                                <strong>Success!</strong> {{ session('success') }}
                            </div>
                        @endif
                        <div class="col-md-4">

                        </div>
                    </div>
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-6 ms-2">
                            <a href="{{ url('/add-user') }}" class="btn btn-outline-dark btn-sm ">tambah</a>
                        </div>
                        <div class="col-md-4 me-3">
                            <form action="">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Search Products"
                                        aria-label="Recipient's username" aria-describedby="button-addon2">
                                    <button class="btn btn-outline-primary mb-0" type="button" id="button-addon2"><i
                                            class="fas fa-search" aria-hidden="true"></i></span></button>
                                </div>
                            </form>


                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            User Name</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Email</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Role</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Action</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $dt)
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="{{ $dt->avatar() }}" class="avatar avatar-sm me-3"
                                                            alt="user1">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <a href="{{ url('show/' . $dt->id . '/product') }}">
                                                            <h6 class="mb-0 text-sm">{{ $dt->name }}</h6>
                                                        </a>
                                                        <p class="text-xs text-secondary mb-0">
                                                            {{ $dt->email }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="text-sm font-weight-bold mb-0">
                                                    {{ $dt->email }}</p>
                                                {{-- <p class="text-xs text-secondary mb-0">Organization</p> --}}
                                            </td>
                                            {{-- <td class="align-middle text-center text-sm">
                                                @if ($dt->status_id == 1)
                                                    <span
                                                        class="badge badge-sm bg-gradient-danger">{{ $dt->status->nama }}</span>
                                                @elseif($dt->status_id == 2)
                                                    <span
                                                        class="badge badge-sm bg-gradient-success">{{ $dt->status->nama }}</span>
                                                @else
                                                    <span
                                                        class="badge badge-sm bg-gradient-warning">{{ $dt->status->nama }}</span>
                                                @endif
                                            </td> --}}
                                            <td class="align-middle text-center">
                                                <span class="text-secondary text-xs font-weight-bold">
                                                    {{ $dt->role->nama }}</span>
                                            </td>
                                            <td class="align-middle text-center">
                                                <a href="{{ url('/edit-user/' . $dt->id) }}"
                                                    class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                                                    data-original-title="Edit user">
                                                    Edit
                                                </a>
                                                |<a href="{{ url('/delete-user/' . $dt->id) }}"
                                                    class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                                                    data-original-title="Edit user"
                                                    onclick="if (! confirm('Yakin ingin menghapus data ini?')) { return false; }">
                                                    Delete
                                                </a>
                                                |<a href="{{ url('/reset-user/' . $dt->id) }}"
                                                    class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                                                    data-original-title="Edit user"
                                                    onclick="if (! confirm('Yakin ingin mereset password account  ini?')) { return false; }">
                                                    Reset
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
