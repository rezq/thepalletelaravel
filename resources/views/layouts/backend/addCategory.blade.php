@extends('layouts.backend.app')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        Add Category
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-9">
                            <hr>
                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="row justify-content-center">
                            <div class="col-8">
                                <form action="{{ route('postCategory') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Category Name</label>
                                        <input class="form-control @error('name')is-invalid @enderror" type="text"
                                            placeholder="Category Name" id="example-text-input" name="name">
                                        @error('name')
                                            <p class="text-sm text-danger"><i>{{ $message }}</i></p>
                                        @enderror
                                    </div>
                                    <div class="row ">
                                        <div class="col-md">
                                            <button type="submit" class="btn btn-primary btn-sm ">Submit</button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
