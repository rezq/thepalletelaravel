@extends('layouts.backend.app')


@section('content')
    <div class="container-fluid py-4">

        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        @if (session('success'))
                            <div class="alert alert-success text-white" role="alert">
                                <strong>Success!</strong> {{ session('success') }}
                            </div>
                        @endif

                        Edit Product
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-9">
                            <hr>
                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="row justify-content-center">
                            <div class="col-8">
                                <form action="{{ url('/updated-order/' . $data->id) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Customer:&nbsp;
                                            {{ $data->user->name }}</label>
                                        <br />
                                        <label for="example-text-input" class="form-control-label">Invoice:&nbsp;
                                            {{ $data->invoice }}</label>
                                        <br />
                                        <label for="example-text-input" class="form-control-label">Tanggal:&nbsp;
                                            {{ $data->tanggal }}</label>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect2">Status Order</label>
                                        <select class="form-select" aria-label="Default select example"
                                            name="status_order">
                                            <option disabled selected value="">Choose Category...</option>

                                            <option {{ $data->status_order == 'UNPAID' ? 'selected' : '' }} ">
                                                    UNPAID</option>
                                                <option {{ $data->status_order == 'PAID' ? 'selected' : '' }} ">
                                                PAID</option>
                                            <option {{ $data->status_order == 'SEND' ? 'selected' : '' }} ">
                                                    SEND</option>
                                                <option {{ $data->status_order == 'FINISH' ? 'selected' : '' }}>
                                                    FINISH</option>
                                            </select>
                                        </div>

                                        <div class="  row d-flex justify-content-end mt-3">
                                                <div class="col-auto">
                                                    <div class="mysub">
                                                        <button type="submit"
                                                            class="btn btn-info btn-sm me-1">Submit</button>
                                                    </div>
                                                </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
