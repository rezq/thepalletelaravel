@extends('layouts.backend.app')
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        Add Product
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-9">
                            <hr>
                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="row justify-content-center">
                            <div class="col-8">
                                <form action="{{ route('postProduct') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="example-text-input" class="form-control-label">Product Name</label>
                                        <input class="form-control" type="text" placeholder="Product Name" required
                                            id="example-text-input" name="nama">
                                    </div>

                                    <div class="form-group">
                                        <label for="example-email-input" class="form-control-label">Price</label>
                                        <input class="form-control" type="text" placeholder="Price" name="price" required
                                            id="example-email-input">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect2">Category</label>
                                        <select class="form-select" aria-label="Default select example" required
                                            name="category_id">
                                            <option disabled selected value="">Choose Category...</option>
                                            @foreach ($data as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Description</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="6"
                                            name="desc" required></textarea>
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="formFile" class="form-label">Upload Image Thumbnail</label>
                                        <input class="form-control" type="file" id="formFile" name="imgthumb">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="formFile" class="form-label">Upload Image 1</label>
                                        <input class="form-control" type="file" id="formFile" name="img1">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="formFile" class="form-label">Upload Image 2</label>
                                        <input class="form-control" type="file" id="formFile" name="img2">
                                    </div>

                                    <!-- <div class="form-group">
                                        <label for="example-tel-input" class="form-control-label">Phone</label>
                                        <input class="form-control" type="tel" value="40-(770)-888-444"
                                            id="example-tel-input" name="phone">
                                    </div> -->
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect2">Status</label>
                                        <select class="form-select" aria-label="Default select example" 
                                        name="status_id" required>
                                            <option disabled selected value="">Choose Status...</option>
                                            @foreach ($status as $stats)
                                                <option value="{{ $stats->id }}">{{ $stats->nama }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="row d-flex justify-content-end mt-3">
                                        <div class="col-auto">
                                            <div class="mysub">
                                                <button type="submit" class="btn btn-info btn-sm me-1">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
