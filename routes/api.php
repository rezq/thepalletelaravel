<?php

use App\Http\Controllers\API\TransactionController;
use App\Http\Controllers\ProductControllers;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/add-to-cart', [TransactionController::class, 'store']);
});

Route::get('/products', [TransactionController::class, 'indexApi']);
Route::get('/list-product', [TransactionController::class, 'listAll']);
Route::get('/list-product/{key}', [TransactionController::class, 'searchApi']);
Route::post('/user-login', [TransactionController::class, 'index']);
Route::post('/user-register', [TransactionController::class, 'newCustomer']);
Route::get('/customer-profile/{id}', [TransactionController::class, 'getProfile']);
Route::post('/customer-profile/{id}', [TransactionController::class,'userProfile']);
route::post('/change-pass/{id}',[TransactionController::class, 'changePass']);
Route::get('/detail/{id}/product', [TransactionController::class, 'showApi']);
Route::get('/products-lainnya', [TransactionController::class, 'randProduct']);
Route::post('/buy-item', [TransactionController::class, 'buyItem']);
Route::get('/order-unpaid/{id}', [TransactionController::class, 'orderUnpaid']);
Route::get('/order-paid/{id}', [TransactionController::class, 'orderPaid']);
Route::get('/order-finish/{id}', [TransactionController::class, 'orderFinish']);
Route::get('/customer-finish/{id}', [TransactionController::class, 'updateSelesai']);
Route::get('/krawangan', [TransactionController::class, 'filterKrawangan']);
Route::get('/lampu', [TransactionController::class, 'filterLampu']);
Route::get('/kantor', [TransactionController::class, 'filterKantor']);
Route::get('/sculpture', [TransactionController::class, 'filterSculpture']);

Route::get('/profile', [TransactionController::class, 'getProfile']);
