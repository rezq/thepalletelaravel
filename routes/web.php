<?php

use App\Http\Controllers\API\TransactionController;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderControllers;
use App\Http\Controllers\ProductControllers;
use App\Http\Controllers\CategoryControllers;
use App\Http\Controllers\DashboardControllers;
use App\Http\Controllers\UserManagementControllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.frontend.index');
});
// Route::get('/dashboard', DashboardControllers::class);


    Route::group(['middleware' => 'auth'], function(){
    Route::get('/dashboard', DashboardControllers::class);

    //Products
    Route::get('/products', [ProductControllers::class, 'index']);
    Route::get('/products/{key}', [ProductControllers::class, 'index']);
    Route::get('/add', [ProductControllers::class, 'create'])->name('addProduct');
    Route::post('/add', [ProductControllers::class, 'store'])->name('postProduct');
    Route::get('/edit/{id}/product', [ProductControllers::class, 'edit']);
    Route::post('/update/{id}/product', [ProductControllers::class,'update']);
    Route::get('/delete/{id}/product', [ProductControllers::class, 'destroy']);
    Route::get('/show/{id}/product', [ProductControllers::class, 'show']);

    //?? Order
    Route::get('/orders', [OrderControllers::class, 'index']);
    Route::get('/orders/{key}', [OrderControllers::class, 'search']);
    Route::get('/confirm-payment/{id}', [OrderControllers::class, 'confirmPay']);
    Route::get('/send-order/{id}', [OrderControllers::class, 'sendOrder']);
    Route::get('/update-order/{id}', [OrderControllers::class, 'edit']);
    Route::post('/updated-order/{id}',[OrderControllers::class, 'update']);
    Route::get('/delete-order/{id}', [OrderControllers::class, 'destroy']);


    //?? User Management
    Route::get('/users', [UserManagementControllers::class, 'index']);
    Route::get('/add-user', [UserManagementControllers::class, 'create']);
    Route::post('/post-user', [UserManagementControllers::class, 'store']);
    Route::get('/edit-user/{id}' , [UserManagementControllers::class, 'edit']);
    Route::post('/update-user/{id}',[UserManagementControllers::class, 'update']);
    Route::get('/delete-user/{id}',[UserManagementControllers::class, 'destroy']);
    Route::get('/reset-user/{id}', [UserManagementControllers::class, 'resetUser']);
    //??Route Category
    Route::get('/category', [CategoryControllers::class, 'index']);
    Route::get('/addCategory', [CategoryControllers::class, 'create']);
    Route::post('/addCategory', [CategoryControllers::class, 'store'])->name('postCategory');
    Route::get('/edit-category/{id}', [CategoryControllers::class, 'edit']);
    Route::post('/update-category/{id}', [CategoryControllers::class, 'update']);
    Route::get('/delete-category/{id}', [CategoryControllers::class, 'destroy']);
    Route::get('/user-profile', [UserManagementControllers::class, 'profile']);

    Route::get('/check/{id}/order', [TransactionController::class, 'show']);
    Route::post('buy-item/{id}', [TransactionController::class, 'buyItem']);
    });

Auth::routes();
Route::get('/logout', function(){
    Auth::logout();
    return redirect('/login');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
